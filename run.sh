docker build -t suedae:v1 .

docker rm -f suedae

docker run --name suedae \
--restart="always" \
--memory=200m \
--cpus=1 \
-e VIRTUAL_HOST=suedae.com,www.suedae.com \
-e LETSENCRYPT_HOST=suedae.com,www.suedae.com \
-e LETSENCRYPT_EMAIL=geek@mounoydev.com \
-d suedae:v1


